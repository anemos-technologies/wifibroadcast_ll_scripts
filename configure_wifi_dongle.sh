#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

source "/root/wifibroadcast_ll_scripts/settings.sh"

VIDEORXRUNNING=`iw dev | grep "center1: $FREQ"`

while true; do
	VIDEORXRUNNING=`iw dev | grep "center1: $FREQ"`
	
	if [[ -z $VIDEORXRUNNING ]]; then
		source "/root/wifibroadcast_ll_scripts/wait_wifi_card.sh"
		
		echo "Setting $CARDRUNNING to frequency $FREQ MHz"
		ifconfig $CARDRUNNING up
		iw dev $CARDRUNNING set bitrates legacy-2.4 24
		ifconfig $CARDRUNNING down
		iw dev $CARDRUNNING set monitor otherbss fcsfail
		ifconfig $CARDRUNNING up
		iw dev $CARDRUNNING set freq $FREQ
		sleep 0.5
	fi
	
	echo "wifi is running"
	
	sleep 0.5
done
