#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

source "/root/wifibroadcast_ll_scripts/settings.sh"

#DISPLAY_PROGRAM="socat -b 1024 - udp4-datagram:$IP_ADDRESS:5000" 


################################# SCRIPT START #######################

while true
do
	source "/root/wifibroadcast_ll_scripts/wait_wifi_card.sh"

	$WBC_PATH/rx $CARDRUNNING
	ps -ef | grep "rx $CARDRUNNING" | grep -v grep | awk '{print $2}' | xargs kill -9
	sleep 0.5
done