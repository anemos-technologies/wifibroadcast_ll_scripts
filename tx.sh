#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

source "/root/wifibroadcast_ll_scripts/settings.sh"


################################# SCRIPT START #######################

while true
do
	source "/root/wifibroadcast_ll_scripts/wait_wifi_card.sh"

	LD_PRELOAD=$WBC_PATH/txhook.so raspivid -cd H264 -pf baseline -b 4500000 -n -g 5 -w 1024 -h 680 -fps 42 -3d sbs -t 0 -o -
	ps -ef | grep "txhook" | grep -v grep | awk '{print $2}' | xargs kill -9
	ps -ef | grep "raspivid" | grep -v grep | awk '{print $2}' | xargs kill -9
	sleep 0.5
done
